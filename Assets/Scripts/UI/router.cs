using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class router : MonoBehaviour
{
    public static router instance;

    public GameObject TapToStartPanel;
    public GameObject PanelWin;
    public GameObject PanelLose;

    float originalSpeed;
    float originalTurnSpeed;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        TapToStartPanel.SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadSceneByIndex(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void nextScene()
    {
        if(SceneManager.GetActiveScene().buildIndex < (SceneManager.sceneCountInBuildSettings-1))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public void tapToStart()
    {
        originalSpeed = FindObjectOfType<Car>().speed;
        originalTurnSpeed = FindObjectOfType<Car>().turnSpeed;
        TapToStartPanel.SetActive(false);
        GameManager.instance.StartPlay();
    }

    public void speedUp(float value)
    {
        

        foreach(Car c in FindObjectsOfType<Car>())
        {
            c.speed = originalSpeed * value;
            c.turnSpeed = originalTurnSpeed * value;
        }
    }

    public void winLevel()
    {
        PanelWin.SetActive(true);
    }

    public void loseLevel()
    {
        PanelLose.SetActive(true);
    }
}
