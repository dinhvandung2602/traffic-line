using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FPSdisplay : MonoBehaviour
{
    TextMeshProUGUI FPStext;

    float pollingTime = 0.2f;
    float time;
    int frameCount;

    // Start is called before the first frame update
    void Start()
    {
        FPStext = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        frameCount++;

        if (time >= pollingTime)
        {
            int frameRate = Mathf.RoundToInt(frameCount / time);
            FPStext.text = frameRate.ToString() + " FPS";

            time -= pollingTime;
            frameCount = 0;
        }
    }
}
