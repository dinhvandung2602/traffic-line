using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CamManager : MonoBehaviour
{
    public static CamManager instance;

    public Transform camDiePivot;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setCamWhenDie(Vector3 pos)
    {
        camDiePivot.GetComponentInChildren<CinemachineVirtualCamera>().Priority = 100;
        camDiePivot.position = pos;
    }
}
