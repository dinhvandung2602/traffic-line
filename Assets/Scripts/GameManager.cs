using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    Car[] cars;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void chooseLevel(int levelIndex)
    {
        LevelManager.instance.loadLevel(levelIndex);
    }

    


    public void StartPlay()
    {
        cars = FindObjectsOfType<Car>();
        foreach (Car c in cars)
        {
            c.Move();
        }
    }

    public bool checkWin()
    {
        for(int i = 0; i < cars.Length; i++)
        {
            if (!cars[i].checkWin())
            {
                Debug.Log("Not win yet");
                return false;
            }
        }
        FindObjectOfType<router>().winLevel();
        return true;
    }
}
