using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public GameObject[] levels;

    public GameObject currentLevel;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void loadLevel(int levelIndex)
    {
        deleteLevel();
        Debug.Log(levelIndex);
        currentLevel = Instantiate(levels[levelIndex], new Vector3(0, 0, 0), Quaternion.identity);
    }

    public void deleteLevel()
    {
        Destroy(currentLevel);
    }
}
