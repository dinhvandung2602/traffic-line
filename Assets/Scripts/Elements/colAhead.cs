using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colAhead : MonoBehaviour
{
    carFindRoad colManager;

    // Start is called before the first frame update
    void Start()
    {
        colManager = GetComponentInParent<carFindRoad>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Road>())
        {
            GetComponentInParent<Car>().FindTarget(other.transform);

            if (other.gameObject.GetComponent<Road>().typeOfRoad == Road.roadType.Straight)
            {
                colManager.setActiveCol("ahead", true);
                colManager.setActiveCol("left", false);
                colManager.setActiveCol("right", false);
                
            }
            else if (other.gameObject.GetComponent<Road>().typeOfRoad == Road.roadType.Curve)
            {
                colManager.setActiveCol("left", true);
                colManager.setActiveCol("right", true);
                colManager.setActiveCol("ahead", false);
            }
            else if (other.gameObject.GetComponent<Road>().typeOfRoad == Road.roadType.Turn)
            {
                colManager.setActiveCol("left", true);
                colManager.setActiveCol("right", true);
                colManager.setActiveCol("ahead", false);

            }


        }
    }
}
