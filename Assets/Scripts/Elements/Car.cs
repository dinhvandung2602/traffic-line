using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    public float speed;
    public float turnSpeed;
    Transform target;
    Transform targetQueue;
    Transform targetPrevious;

    bool canMove;
    bool reachTarget;
    bool reachTargetRotation;
    bool carWin;

    public enum carColor
    {
        Red,
        Orange,
        Yellow,
        Green,
        Cyan,
        Blue,
    }

    public carColor colorOfCar;

    // Start is called before the first frame update
    void Start()
    {
        canMove = false;
        reachTarget = false;
        reachTargetRotation = false;
        carWin = false;
        target = transform;
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        if (canMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            LookAtTarget();

            if (Vector3.Distance(transform.position, target.position) < 0.001f)
            {
                ReachTarget();
            }
            
        }


        if (reachTarget)
        {
            ResetTarget();
            reachTargetRotation = false;
        }

        if (!reachTargetRotation)
        {
            CheckReachTargetRotation();
        }
    }

    public void Move()
    {
        target = targetQueue;
        StartCoroutine(delayFirstMove());
    }

    IEnumerator delayFirstMove()
    {
        yield return new WaitForSeconds(0.2f);
        canMove = true;
    }

    public void LookAtTarget()
    {
        Vector3 targetDirection = target.position - transform.position;

        // The step size is equal to speed times frame time.
        float singleStep = turnSpeed * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singleStep, 0.0f);

        // Draw a ray pointing at our target in
        Debug.DrawRay(transform.position, newDirection, Color.red);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    public void FindTarget(Transform newTarget)
    {
        targetQueue = newTarget;
    }

    public void ReachTarget()
    {
        transform.position = target.position;
        reachTarget = true;
    }

    public void CheckReachTargetRotation()
    {
        if (Vector3.Angle(transform.forward, target.position - transform.position) <= 0.2f)
        {
            
            GetComponentInChildren<carFindRoad>().setActiveCol("ahead", true);
            GetComponentInChildren<carFindRoad>().setActiveCol("left", false);
            GetComponentInChildren<carFindRoad>().setActiveCol("right", false);

            reachTargetRotation = true;
        }
    }


    public void ResetTarget()
    {
        targetPrevious = target;
        target = targetQueue;
        reachTarget = false;

    }

    public void Die()
    {
        router.instance.loseLevel();
        CamManager.instance.setCamWhenDie(gameObject.transform.position);
        speed = 0;
    }

    public void Win()
    {
        carWin = true;
        speed = 0;
        GameManager.instance.checkWin();
    }

    public bool checkWin()
    {
        return carWin;
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.tag == "ColliderDie") || (other.tag == "Car"))
        {
            Die();
        }else if (other.tag == "Reverse")
        {

            target = targetPrevious;

        }
        else if (other.tag == "Home")
        {
            if ((int)colorOfCar == (int)other.GetComponent<Home>().colorOfHome)
            {
                Win();
                other.GetComponent<Home>().carGetHome();
            }
            else
            {
                Die();
            }
        }
    }

}
