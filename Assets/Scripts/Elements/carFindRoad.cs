using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carFindRoad : MonoBehaviour
{
    GameObject col_ahead;
    GameObject col_left;
    GameObject col_right;

    // Start is called before the first frame update
    void Start()
    {
        col_ahead = GetComponentInChildren<colAhead>().gameObject;
        col_left = GetComponentInChildren<colLeft>().gameObject;
        col_right = GetComponentInChildren<colRight>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setActiveCol(string name,bool isActive)
    {
        switch (name)
        {
            case "ahead":
                col_ahead.SetActive(isActive);
                break;
            case "left":
                col_left.SetActive(isActive);
                break;
            case "right":
                col_right.SetActive(isActive);
                break;
        }
    }

}
