using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class roadTurnStraightClick : MonoBehaviour
{
    public Transform parent;
    int currentPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        CheckPos();
        SetPos(currentPos);
    }

    public void CheckPos()
    {
        float yValue = parent.transform.eulerAngles.y;
        
        switch (Mathf.Round(yValue))
        {
            case 0:
                currentPos = 1;
                break;
            case 90:
                currentPos = 2;
                break;
        }
    }

    public void SetPos(int index)
    {
        switch (index)
        {
            case 1:
                parent.transform.DORotate(new Vector3(0, 90, 0), 0.07f);
                currentPos = 2;
                break;
            case 2:
                parent.transform.DORotate(new Vector3(0, 0, 0), 0.07f);
                currentPos = 1;
                break;
        }
    }
}
