using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class roadSwitchClick : MonoBehaviour
{
    public GameObject roadVisual;
    public GameObject colliderDie;

    int currentPos;

    // Start is called before the first frame update
    void Start()
    {
        CheckPos();
        SetPos(currentPos);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPos(int index)
    {
        switch (index)
        {
            case 1:
                roadVisual.transform.DOLocalMove(new Vector3(0, 0, 0), 0.07f);
                colliderDie.transform.localPosition = new Vector3(0, 0, -1);
                currentPos = 1;
                break;
            case 2:
                roadVisual.transform.DOLocalMove(new Vector3(0, 0, -1), 0.07f);
                colliderDie.transform.localPosition = new Vector3(0, 0, 0);
                currentPos = 2;
                break;
        }
    }

    public void CheckPos()
    {
        float zValue = roadVisual.transform.localPosition.z;
        switch (zValue)
        {
            case 0:
                currentPos = 1;
                break;
            case -1:
                currentPos = 2;
                break;
        }
    }

    private void OnMouseDown()
    {
        if (currentPos == 1)
        {
            SetPos(2);
        }else if (currentPos == 2)
        {
            SetPos(1);
        }
    }
}
