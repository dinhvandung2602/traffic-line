using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class roadTurnClick : MonoBehaviour
{
    public Transform parent;
    public GameObject cornerGuide;
    public LineRenderer lineGuide;

    Vector3 startPos;
    Vector3 endPos;
    bool isOnDragging = false;

    // Start is called before the first frame update
    void Start()
    {
        cornerGuide.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (isOnDragging)
            {
                isOnDragging = false;
                endPos = Input.mousePosition;
                cornerGuide.SetActive(false);
                calculateAngle();
            }
        }

        if (isOnDragging)
        {
            endPos = Input.mousePosition;

            lineGuide.SetPosition(0, lineGuide.gameObject.transform.position);
            lineGuide.SetPosition(1, Camera.main.ScreenToWorldPoint(Input.mousePosition));
            calculateAngle();
        }
    }

    private void OnMouseDown()
    {
        cornerGuide.SetActive(true);
        startPos = Input.mousePosition;
        isOnDragging = true;
    }

    public void calculateAngle()
    {
        Vector3 angleVector = endPos - startPos;
        if((angleVector.x>0)&& (angleVector.y > 0))
        {
            //parent.transform.localEulerAngles = new Vector3(0, 0, 0);
            parent.transform.DORotate(new Vector3(0, 0, 0), 0.07f);
        }
        if ((angleVector.x > 0) && (angleVector.y < 0))
        {
            //parent.transform.localEulerAngles = new Vector3(0, 90, 0);
            parent.transform.DORotate(new Vector3(0, 90, 0), 0.07f);
        }
        if ((angleVector.x < 0) && (angleVector.y < 0))
        {
            //parent.transform.localEulerAngles = new Vector3(0, 180, 0);
            parent.transform.DORotate(new Vector3(0, 180, 0), 0.07f);
        }
        if ((angleVector.x < 0) && (angleVector.y > 0))
        {
            //parent.transform.localEulerAngles = new Vector3(0, 270, 0);
            parent.transform.DORotate(new Vector3(0, 270, 0), 0.07f);
        }

    }
}
