using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Home : MonoBehaviour
{
    public enum homeColor
    {
        Red,
        Orange,
        Yellow,
        Green,
        Cyan,
        Blue,
    }

    public homeColor colorOfHome;

    public GameObject doneMark;

    // Start is called before the first frame update
    void Start()
    {
        doneMark.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void carGetHome()
    {
        doneMark.SetActive(true);
    }
}
