using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colRight : MonoBehaviour
{
    carFindRoad colManager;
    public GameObject colLeft;

    // Start is called before the first frame update
    void Start()
    {
        colManager = GetComponentInParent<carFindRoad>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ColliderTurn")
        {
            colLeft.SetActive(false);
        }

        if (other.gameObject.GetComponent<Road>())
        {
            GetComponentInParent<Car>().FindTarget(other.transform);
            colLeft.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
