using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAtPlayer : MonoBehaviour
{
    Camera player;
    public Vector3 offsetLookAt;

    // Start is called before the first frame update
    void Start()
    {
        player = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.LookAt(player.transform);
        //offset
        transform.eulerAngles += offsetLookAt;
    }
}
